#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands


"""Copy Special exercise
"""

def get_special_paths(dir):
  file_name=[]
  file_list=os.listdir(dir)
  for file in file_list:
    if re.search(r'__(\w+)__', file):
      file_name.append(os.path.abspath(file))
  return file_name


def copy_to(paths,dir):

  if not os.path.exists(dir):
    os.mkdir(dir)

  for path in paths:
      shutil.copy2(path,dir)



def zip_to(paths, zipfile):
  """Zip up all of the given files into a new zip file with the given name."""

  for path in paths:
      cmd = 'c:/zip/zip -j ' + zipfile + ' ' + path
      print "Command I'm going to do:" + cmd

      if os.system(cmd) == 0:
        print 'Ok'
      else:
        sys.exit(1)



def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)

  paths = []
  for dir in args:
    paths.extend(get_special_paths(dir))

  if todir:
    copy_to(paths, todir)
  elif tozip:
    zip_to(paths, tozip)
  else:
    print '\n'.join(paths)
  
if __name__ == "__main__":
  main()
